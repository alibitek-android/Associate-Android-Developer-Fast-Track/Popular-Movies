package ro.linuxgeek.popularmovies.data;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.linuxgeek.popularmovies.R;
import ro.linuxgeek.popularmovies.network.NetworkUtils;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ReviewViewHolder> {

    private Review[] mReviews;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private final ReviewClickListener mReviewClickListener;

    private int mExpandedPosition = RecyclerView.NO_POSITION;

    public interface ReviewClickListener {
        void onReviewClicked(Review review);
    }

    public void setReviews(Review[] reviews) {
        mReviews = reviews;
        notifyDataSetChanged();
    }

    public ReviewsAdapter(Context context, ReviewClickListener reviewClickListener, RecyclerView recyclerView) {
        mContext = context;
        mReviewClickListener = reviewClickListener;
        mRecyclerView = recyclerView;
    }

    @Override
    public ReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_row_item, parent, false);
        return new ReviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ReviewViewHolder holder, final int position) {
        holder.bindData(position);
        final boolean isExpanded = position == mExpandedPosition;

        holder.preview.setVisibility(isExpanded ? View.GONE : View.VISIBLE);
        holder.details.setVisibility(isExpanded ? View.VISIBLE : View.GONE);

        holder.itemView.setActivated(isExpanded);
    }

    @Override
    public int getItemCount() {
        return mReviews != null ? mReviews.length : 0;
    }

    class ReviewViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tv_review_preview)
        TextView preview;

        @BindView(R.id.tv_review_details)
        TextView details;

        ReviewViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();

            final boolean isExpanded = position == mExpandedPosition;

            mExpandedPosition = isExpanded ? -1 : position;
            TransitionManager.beginDelayedTransition(mRecyclerView);
            notifyItemChanged(position);

            if (position != RecyclerView.NO_POSITION)
                mReviewClickListener.onReviewClicked(mReviews[position]);
        }

        void bindData(int position) {
            Review review = mReviews[position];
            String content = review.getContent();
            int lastIndex = 80;
            String previewText = content.length() > lastIndex ? content.substring(0, lastIndex) + "..." : content;
            preview.setText(previewText);
            details.setText(content);
        }
    }
}
