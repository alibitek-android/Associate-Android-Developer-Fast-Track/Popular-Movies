package ro.linuxgeek.popularmovies.data;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.linuxgeek.popularmovies.R;

public class FavoriteMoviesAdapter extends CursorRecyclerViewAdapter<FavoriteMoviesAdapter.FavoriteMoviesViewHolder> {
    private static final String TAG = FavoriteMoviesAdapter.class.getSimpleName();
    private final MoviesAdapter.MovieClickListener mMovieClickListener;
    private Context mContext;

    public FavoriteMoviesAdapter(Context context,
                                 Cursor cursor,
                                 MoviesAdapter.MovieClickListener movieClickListener) {
        super(context, cursor);
        mContext = context;
        mMovieClickListener = movieClickListener;
    }

    class FavoriteMoviesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.iv_movie_poster)
        ImageView mImageView;

        Movie movie;

        FavoriteMoviesViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION)
                mMovieClickListener.onMovieClicked(movie);
        }

        void bindData(Cursor cursor) {
            byte[] posterData = cursor.getBlob(cursor.getColumnIndex(FavoriteMoviesColumns.POSTER_DATA));
            mImageView.setImageDrawable(new BitmapDrawable(mContext.getResources(), BitmapFactory.decodeByteArray(posterData, 0, posterData.length)));

            movie = Movie.fromCursor(cursor);
        }
    }

    @Override
    public FavoriteMoviesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_row_item, parent, false);
        return new FavoriteMoviesAdapter.FavoriteMoviesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FavoriteMoviesViewHolder viewHolder, Cursor cursor) {
        //DatabaseUtils.dumpCursor(cursor);
        viewHolder.bindData(cursor);
    }
}
