package ro.linuxgeek.popularmovies.data;

import net.simonvt.schematic.annotation.Database;
import net.simonvt.schematic.annotation.Table;

@Database(version = FavoriteMoviesDatabase.VERSION,
        packageName = "ro.linuxgeek.popularmovies.data.provider")
public class FavoriteMoviesDatabase {
    private FavoriteMoviesDatabase() {}

    public static final int VERSION = 2;

    @Table(FavoriteMoviesColumns.class)
    public static final String FAVORITES = "favorites";
}
