package ro.linuxgeek.popularmovies.data;

import android.net.Uri;

import net.simonvt.schematic.annotation.ContentProvider;
import net.simonvt.schematic.annotation.ContentUri;
import net.simonvt.schematic.annotation.InexactContentUri;
import net.simonvt.schematic.annotation.TableEndpoint;

@ContentProvider(
        authority = FavoriteMoviesProvider.AUTHORITY,
        database = FavoriteMoviesDatabase.class,
        packageName = "ro.linuxgeek.popularmovies.data.provider")
public class FavoriteMoviesProvider {
    public static final String AUTHORITY = "ro.linuxgeek.popularmovies.data.FavoriteMoviesProvider";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    interface Path {
        String FAVORITE_MOVIES = "favorite_movies";
    }

    private static Uri buildUri(String... paths){
        Uri.Builder builder = BASE_CONTENT_URI.buildUpon();

        for (String path : paths){
            builder.appendPath(path);
        }

        return builder.build();
    }

    @TableEndpoint(table = FavoriteMoviesDatabase.FAVORITES)
    public static class FavoriteMovies
    {
        @ContentUri(
                path = Path.FAVORITE_MOVIES,
                type = "vnd.android.cursor.dir/favorite_movies",
                defaultSort = FavoriteMoviesColumns.RELEASE_DATE + " DESC")
        public static final Uri CONTENT_URI = buildUri(Path.FAVORITE_MOVIES);

        @InexactContentUri(
                path = Path.FAVORITE_MOVIES + "/#",
                name = "FAVORITE_MOVIE_ID",
                type = "vnd.android.cursor.item/favorite_movie",
                whereColumn = FavoriteMoviesColumns.ID,
                pathSegment = 1)
        public static Uri withId(long id) {
            return buildUri(Path.FAVORITE_MOVIES, String.valueOf(id));
        }
    }
}
