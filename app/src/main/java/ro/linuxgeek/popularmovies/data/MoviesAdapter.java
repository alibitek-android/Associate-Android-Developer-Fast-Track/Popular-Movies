package ro.linuxgeek.popularmovies.data;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.linuxgeek.popularmovies.network.NetworkUtils;
import ro.linuxgeek.popularmovies.R;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieViewHolder> {

    private Movie[] mMovies;
    private Context mContext;
    private final MovieClickListener mMovieClickListener;

    public interface MovieClickListener {
        void onMovieClicked(Movie movie);
    }

    public void setMovies(Movie[] movies) {
        mMovies = movies;
        notifyDataSetChanged();
    }

    public MoviesAdapter(Context context, MovieClickListener movieClickListener) {
        mContext = context;
        mMovieClickListener = movieClickListener;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_row_item, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        holder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return mMovies != null ? mMovies.length : 0;
    }

    class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.iv_movie_poster)
        ImageView mImageView;

        MovieViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION)
                mMovieClickListener.onMovieClicked(mMovies[position]);
        }

        void bindData(int position) {
            NetworkUtils.fetchAndLoadImage(mContext
                    , NetworkUtils.getFullImageUrl(mMovies[position].getPosterPath())
                    , mImageView);
        }
    }
}
