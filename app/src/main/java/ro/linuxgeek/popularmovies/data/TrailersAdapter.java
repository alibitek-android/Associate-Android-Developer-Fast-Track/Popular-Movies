package ro.linuxgeek.popularmovies.data;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.linuxgeek.popularmovies.R;

public class TrailersAdapter extends RecyclerView.Adapter<TrailersAdapter.TrailerViewHolder> {

    private static final String TAG = TrailersAdapter.class.getSimpleName();
    private Trailer[] mTrailers;
    private Context mContext;
    private final TrailerClickListener mTrailerClickListener;

    public interface TrailerClickListener {
        void onTrailerClicked(Trailer trailer);
    }

    public void setTrailers(Trailer[] trailers) {
        mTrailers = trailers;
        notifyDataSetChanged();
    }

    public TrailersAdapter(Context context, TrailerClickListener trailerClickListener) {
        mContext = context;
        mTrailerClickListener = trailerClickListener;
    }

    @Override
    public TrailerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.trailer_row_item, parent, false);
        return new TrailerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TrailerViewHolder holder, int position) {
        holder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return mTrailers != null ? mTrailers.length : 0;
    }

    class TrailerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tv_trailer_name)
        TextView mTrailerName;

        TrailerViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION)
                mTrailerClickListener.onTrailerClicked(mTrailers[position]);
        }

        void bindData(int position) {
            String name = mTrailers[position].getName();
            Log.d(TAG, "Trailer name: " + name);
            mTrailerName.setText(name);
        }
    }
}
