package ro.linuxgeek.popularmovies.data;

import net.simonvt.schematic.annotation.AutoIncrement;
import net.simonvt.schematic.annotation.DataType;
import net.simonvt.schematic.annotation.NotNull;
import net.simonvt.schematic.annotation.PrimaryKey;
import net.simonvt.schematic.annotation.Unique;

public interface FavoriteMoviesColumns {

    @DataType(DataType.Type.INTEGER)
    @PrimaryKey
    @AutoIncrement
    String _ID = "_id";

    @DataType(DataType.Type.INTEGER)
    @Unique
    public static final String ID = "id";

    @DataType(DataType.Type.TEXT)
    @NotNull
    public static final String TITLE = "title";

    @DataType(DataType.Type.TEXT)
    @NotNull
    public static final String DESCRIPTION = "description";

    @DataType(DataType.Type.REAL)
    @NotNull public static final String RATING = "rating";

    @DataType(DataType.Type.INTEGER)
    @NotNull public static final String RELEASE_DATE = "release_date";

    @DataType(DataType.Type.TEXT)
    @NotNull
    public static final String POSTER_PATH = "poster_path";

    @DataType(DataType.Type.BLOB)
    @NotNull
    public static final String POSTER_DATA = "poster_data";

    @DataType(DataType.Type.INTEGER)
    @NotNull
    public static final String FAVORITE_TIME = "favorite_time";
}
