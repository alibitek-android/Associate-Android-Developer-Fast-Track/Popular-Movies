package ro.linuxgeek.popularmovies.data;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Movie implements Parcelable {
    private static final String TAG = Movie.class.getSimpleName();

    public static final String MOVIE_PARCELABLE_KEY = "MOVIE";

    private int id;
    private String poster_path;
    private String original_title;
    private String overview;
    private float vote_average;
    private Date release_date;

    // <Parcelable>
    private Movie(Parcel in) {
        id = in.readInt();
        poster_path = in.readString();
        original_title = in.readString();
        overview = in.readString();
        vote_average = in.readFloat();
        release_date = new Date(in.readLong());
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(poster_path);
        dest.writeString(original_title);
        dest.writeString(overview);
        dest.writeFloat(vote_average);
        dest.writeLong(release_date.getTime());
    }
    // </Parcelable>

    Movie() {

    }

    public Movie(int id, String posterPath, String originalTitle, String overview, float rating, Date releaseDate) {
        this.id = id;
        this.poster_path = posterPath;
        this.original_title = originalTitle;
        this.overview = overview;
        this.vote_average = rating;
        this.release_date = releaseDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPosterPath() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getOriginalTitle() {
        return original_title;
    }

    public void setOriginal_title(String original_title) {
        this.original_title = original_title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public float getRating() {
        return vote_average;
    }

    public void setVote_average(float vote_average) {
        this.vote_average = vote_average;
    }

    public Date getReleaseDate() {
        return release_date;
    }

    public void setRelease_date(Date release_date) {
        this.release_date = release_date;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", poster_path='" + poster_path + '\'' +
                ", original_title='" + original_title + '\'' +
                ", overview='" + overview + '\'' +
                ", vote_average='" + vote_average + '\'' +
                ", release_date='" + release_date + '\'' +
                '}';
    }

    public static Movie fromCursor(Cursor cursor) {
        return new Movie(
            cursor.getInt(cursor.getColumnIndex(FavoriteMoviesColumns.ID)),
            cursor.getString(cursor.getColumnIndex(FavoriteMoviesColumns.POSTER_PATH)),
            cursor.getString(cursor.getColumnIndex(FavoriteMoviesColumns.TITLE)),
            cursor.getString(cursor.getColumnIndex(FavoriteMoviesColumns.DESCRIPTION)),
            cursor.getFloat(cursor.getColumnIndex(FavoriteMoviesColumns.RATING)),
            new Date((long) cursor.getInt(cursor.getColumnIndex(FavoriteMoviesColumns.RELEASE_DATE)) * 1000)
        );
    }
}
