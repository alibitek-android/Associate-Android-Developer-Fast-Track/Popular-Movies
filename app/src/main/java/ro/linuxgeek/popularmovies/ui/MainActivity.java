package ro.linuxgeek.popularmovies.ui;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import java.net.URL;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import ro.linuxgeek.popularmovies.R;
import ro.linuxgeek.popularmovies.data.FavoriteMoviesAdapter;
import ro.linuxgeek.popularmovies.data.FavoriteMoviesProvider;
import ro.linuxgeek.popularmovies.data.Movie;
import ro.linuxgeek.popularmovies.data.MoviesAdapter;
import ro.linuxgeek.popularmovies.network.NetworkUtils;

public class MainActivity extends AppCompatActivity
        implements
        MoviesAdapter.MovieClickListener,
        LoaderManager.LoaderCallbacks<Movie[]> {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int SHOW_MOST_POPULAR = 1;
    private static final int SHOW_TOP_RATED = 2;
    private static final int SHOW_FAVORITES = 3;

    @BindView(R.id.tv_error)
    TextView mErrorTextView;

    @BindView(R.id.pb_loading)
    ProgressBar mProgressBar;

    @BindView(R.id.rv_movie_posters)
    RecyclerView mMoviesRecyclerView;

    private MoviesAdapter mMoviesAdapter;
    private FavoriteMoviesAdapter mFavoriteMoviesAdapter;
    private FavoriteMoviesLoader mFavoriteMoviesLoader = new FavoriteMoviesLoader();

    private static final String SHOW_METHOD_KEY = "sort_method";
    private static int mShowMethod = SHOW_MOST_POPULAR;

    private static final int MOVIES_LOADER_ID = 1;
    private static final int FAVORITE_MOVIES_LOADER_ID = 2;
    private static final String MOVIES_URL_EXTRA = "movies_url";

    @Override
    public void onMovieClicked(Movie movie) {
        Intent movieDetailsIntent = new Intent(this, DetailsActivity.class);
        movieDetailsIntent.putExtra(Movie.MOVIE_PARCELABLE_KEY, movie);
        startActivity(movieDetailsIntent);
    }

    private enum Response
    {
        SUCCESS,
        INVALID_ARGUMENT,
        INVALID_URL,
        NO_INTERNET
    }

    public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {
        private int mItemOffset;

        public ItemOffsetDecoration(int itemOffset) {
            mItemOffset = itemOffset;
        }

        public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
            this(context.getResources().getDimensionPixelSize(itemOffsetId));
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(SHOW_METHOD_KEY, mShowMethod);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        mMoviesRecyclerView.setLayoutManager(gridLayoutManager);

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(this, R.dimen.item_offset);
        mMoviesRecyclerView.addItemDecoration(itemDecoration);

        mMoviesRecyclerView.setHasFixedSize(true);

        if (savedInstanceState != null && savedInstanceState.containsKey(SHOW_METHOD_KEY)) {
            mShowMethod = savedInstanceState.getInt(SHOW_METHOD_KEY);
        }

        mFavoriteMoviesAdapter = new FavoriteMoviesAdapter(this, null, this);
        mMoviesAdapter = new MoviesAdapter(this, this);

        if (mShowMethod == SHOW_FAVORITES) {
            showFavorites();
        }
        else {
            mMoviesRecyclerView.setAdapter(mMoviesAdapter);
            handleResponse(fetchMovies(mShowMethod));
        }
    }

    private Response fetchMovies(int sortOption) {
        Log.d(TAG, "fetchMovies: " + String.valueOf(sortOption));

        if (sortOption != SHOW_MOST_POPULAR && sortOption != SHOW_TOP_RATED) {
            Log.e(TAG, "Unknown sort option provided: " + String.valueOf(sortOption));
            return Response.INVALID_ARGUMENT;
        }

        if (!NetworkUtils.isOnline(this))
            return Response.NO_INTERNET;

        URL url;

        if (sortOption == SHOW_MOST_POPULAR)
            url = NetworkUtils.getPopularMoviesUrl();
        else
            url = NetworkUtils.getTopRatedMoviesUrl();

        if (url != null) {
            if (mMoviesRecyclerView.getAdapter() instanceof MoviesAdapter)
                invalidateData();
            else
                mMoviesRecyclerView.setAdapter(mMoviesAdapter);

            Bundle queryBundle = new Bundle();
            queryBundle.putString(MOVIES_URL_EXTRA, url.toString());

            getSupportLoaderManager().restartLoader(MOVIES_LOADER_ID, queryBundle, this);

            return Response.SUCCESS;
        }

        return Response.INVALID_URL;
    }

    private void invalidateData() {
        mMoviesAdapter.setMovies(null);
    }

    public void showMovies(Movie[] movies) {
        //Log.d(TAG, "Showing movies: " + movies.length + " " + Arrays.toString(movies));
        mErrorTextView.setVisibility(View.INVISIBLE);
        mMoviesRecyclerView.setVisibility(View.VISIBLE);
        mMoviesAdapter.setMovies(movies);
    }

    public void showErrorMessage() {
        //Log.d(TAG, "Showing error");
        mMoviesRecyclerView.setVisibility(View.INVISIBLE);
        mErrorTextView.setVisibility(View.VISIBLE);
    }

    private class MoviesResult
    {
        Movie[] results;
    }

    @Override
    public Loader<Movie[]> onCreateLoader(int id, final Bundle args) {
        return new AsyncTaskLoader<Movie[]>(this) {

            private Map<Integer, Movie[]> mCachedMovies = new LinkedHashMap<>();

            @Override
            protected void onStartLoading() {
                if (args == null) {
                    Log.d(TAG, "onStartLoading - no args");
                    return;
                }

                Log.d(TAG, mCachedMovies.keySet().toString());

                if (mCachedMovies == null || !mCachedMovies.containsKey(mShowMethod)) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    Log.d(TAG, "onStartLoading - no cache");
                    forceLoad();
                }
                else {
                    Log.d(TAG, "onStartLoading - cache");
                    deliverResult(mCachedMovies.get(mShowMethod));
                }
            }

            @Override
            public Movie[] loadInBackground() {
                Log.d(TAG, "loadInBackground");

                String url = args.getString(MOVIES_URL_EXTRA);

                if (TextUtils.isEmpty(url)) {
                    return null;
                }

                try {
                    return new Gson().fromJson(NetworkUtils.getResponseFromHttpUrl(new URL(url)), MoviesResult.class).results;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public void deliverResult(Movie[] movies) {
                Log.d(TAG, "deliverResult: " + mShowMethod);
                mCachedMovies.put(mShowMethod, movies);
                super.deliverResult(movies);
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<Movie[]> loader, Movie[] movies) {
        Log.d(TAG, "onLoadFinished");

        mProgressBar.setVisibility(View.INVISIBLE);

        if (movies != null) {
            showMovies(movies);
        } else {
            showErrorMessage();
        }
    }

    @Override
    public void onLoaderReset(Loader<Movie[]> loader) {
        Log.d(TAG, "onLoaderReset");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.action_most_popular) {
            handleResponse(fetchMovies(mShowMethod = SHOW_MOST_POPULAR));
            return true;
        } else if (itemId == R.id.action_top_rated) {
            handleResponse(fetchMovies(mShowMethod = SHOW_TOP_RATED));
            return true;
        } else if (itemId == R.id.action_favorites) {
            mShowMethod = SHOW_FAVORITES;
            showFavorites();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void handleResponse(Response response) {
        if (response != Response.SUCCESS) {
            showErrorMessage();
        }
    }

    private void showFavorites() {
        Log.d(TAG, "Showing favorite movies!");

        mMoviesRecyclerView.setAdapter(mFavoriteMoviesAdapter);

        getSupportLoaderManager().restartLoader(FAVORITE_MOVIES_LOADER_ID, null, mFavoriteMoviesLoader);
    }

    @Override
    public void onRestart() {
        super.onRestart();

        if (mShowMethod == SHOW_FAVORITES)
            getSupportLoaderManager().restartLoader(FAVORITE_MOVIES_LOADER_ID, null, mFavoriteMoviesLoader);
    }

    private class FavoriteMoviesLoader implements LoaderManager.LoaderCallbacks<Cursor> {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            Log.d(TAG, "onCreateLoader - FavoriteMoviesLoader");
            mProgressBar.setVisibility(View.VISIBLE);

            return new CursorLoader(MainActivity.this,
                                    FavoriteMoviesProvider.FavoriteMovies.CONTENT_URI,
                                    null,
                                    null,
                                    null,
                                    null);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            Log.d(TAG, "onLoadFinished - FavoriteMoviesLoader");
            mProgressBar.setVisibility(View.INVISIBLE);
            mFavoriteMoviesAdapter.swapCursor(data);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            Log.d(TAG, "onLoaderReset - FavoriteMoviesLoader");
            mFavoriteMoviesAdapter.swapCursor(null);
        }
    }
}
