package ro.linuxgeek.popularmovies.ui;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ro.linuxgeek.popularmovies.PopularMovies;
import ro.linuxgeek.popularmovies.R;
import ro.linuxgeek.popularmovies.data.FavoriteMoviesColumns;
import ro.linuxgeek.popularmovies.data.FavoriteMoviesProvider;
import ro.linuxgeek.popularmovies.data.Movie;
import ro.linuxgeek.popularmovies.data.Review;
import ro.linuxgeek.popularmovies.data.ReviewsAdapter;
import ro.linuxgeek.popularmovies.data.Trailer;
import ro.linuxgeek.popularmovies.data.TrailersAdapter;
import ro.linuxgeek.popularmovies.network.NetworkUtils;

public class DetailsActivity extends AppCompatActivity implements TrailersAdapter.TrailerClickListener, ReviewsAdapter.ReviewClickListener {
    private static final String TAG = DetailsActivity.class.getSimpleName();

    @BindView(R.id.tv_original_title)
    TextView mOriginalTitle;

    @BindView(R.id.iv_poster)
    ImageView mMoviePoster;

    @BindView(R.id.tv_plot_synopsis)
    TextView mPlotSynopsis;

    @BindView(R.id.rb_user_rating)
    RatingBar mUserRating;

    @BindView(R.id.tv_release_Date)
    TextView mReleaseDate;

    @BindView(R.id.tv_details_error)
    TextView mErrorTextView;

    @BindView(R.id.pb_details_loading)
    ProgressBar mLoading;

    @BindView(R.id.rv_movie_reviews)
    RecyclerView mReviewsRecyclerView;

    @BindView(R.id.rv_movie_trailers)
    RecyclerView mTrailersRecyclerView;

    @BindView(R.id.mark_as_favorite)
    CheckBox mFavorite;

    @BindView(R.id.tv_reviews_header)
    TextView mReviewsHeader;

    private TrailersAdapter mTrailersAdapter;
    private ReviewsAdapter mReviewsAdapter;

    private Movie mMovie;

    private static final int MOVIE_REVIEWS_LOADER = 1;
    private static final int MOVIE_TRAILERS_LOADER = 2;
    private static final String MOVIE_TRAILERS_URL_EXTRA = "MOVIE_TRAILERS_URL";
    private static final String MOVIE_REVIEWS_URL_EXTRA = "MOVIE_TRAILERS_URL";

    private TrailersLoader mTrailersLoader = new TrailersLoader();
    private ReviewsLoader mReviewsLoader = new ReviewsLoader();

    private static final Gson mGson = new Gson();

    public void showErrorMessage(int id) {
        Log.d(TAG, "Showing error");

        mErrorTextView.setVisibility(View.INVISIBLE);

        if (id == MOVIE_REVIEWS_LOADER) {
            mReviewsRecyclerView.setVisibility(View.INVISIBLE);
        }
        else {
            mTrailersRecyclerView.setVisibility(View.INVISIBLE);
        }
    }

    public void showTrailers(Trailer[] trailers) {
        //Log.d(TAG, "Showing trailers: " + trailers.length + " " + Arrays.toString(trailers));
        mErrorTextView.setVisibility(View.INVISIBLE);
        mTrailersRecyclerView.setVisibility(View.VISIBLE);
        mTrailersAdapter.setTrailers(trailers);
    }

    public void showReviews(Review[] reviews) {
        //Log.d(TAG, "Showing reviews: " + reviews.length + " " + Arrays.toString(reviews));
        mErrorTextView.setVisibility(View.INVISIBLE);
        mReviewsRecyclerView.setVisibility(View.VISIBLE);
        mReviewsAdapter.setReviews(reviews);
        if (reviews.length == 0) {
            mReviewsHeader.setVisibility(View.GONE);
        } else {
            mReviewsHeader.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onTrailerClicked(Trailer trailer) {
        viewTrailer(trailer.getKey());
    }

    @Override
    public void onReviewClicked(Review review) {
        //Log.d(TAG, "Tapped review: " + review.getContent());
    }

    private class ReviewsLoader implements LoaderManager.LoaderCallbacks<Review[]> {

        private class ReviewsResult
        {
            Review[] results;
        }

        @Override
        public Loader<Review[]> onCreateLoader(int id, final Bundle args) {
            return new AsyncTaskLoader<Review[]>(DetailsActivity.this) {

                private Review[] mCachedReviews;

                @Override
                protected void onStartLoading() {
                    if (args == null) {
                        Log.d(TAG, "ReviewsLoader - onStartLoading - no args");
                        return;
                    }

                    if (mCachedReviews == null) {
                        mLoading.setVisibility(View.VISIBLE);
                        Log.d(TAG, "ReviewsLoader - onStartLoading - no cache");
                        forceLoad();
                    }
                    else {
                        Log.d(TAG, "ReviewsLoader - onStartLoading - cache");
                        deliverResult(mCachedReviews);
                    }
                }

                @Override
                public Review[] loadInBackground() {
                    Log.d(TAG, "ReviewsLoader - loadInBackground");

                    String url = args.getString(MOVIE_REVIEWS_URL_EXTRA);

                    if (TextUtils.isEmpty(url)) {
                        return null;
                    }

                    try {
                        return mGson.fromJson(NetworkUtils.getResponseFromHttpUrl(new URL(url)), ReviewsResult.class).results;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                public void deliverResult(Review[] reviews) {
                    Log.d(TAG, "ReviewsLoader - deliverResult: ");
                    mCachedReviews = reviews;
                    super.deliverResult(mCachedReviews);
                }
            };
        }

        @Override
        public void onLoadFinished(Loader<Review[]> loader, Review[] reviews) {
            Log.d(TAG, "onLoadFinished");

            mLoading.setVisibility(View.INVISIBLE);

            if (reviews != null) {
                showReviews(reviews);
            } else {
                showErrorMessage(MOVIE_TRAILERS_LOADER);
            }
        }

        @Override
        public void onLoaderReset(Loader<Review[]> loader) {

        }
    }

    private class TrailersLoader implements LoaderManager.LoaderCallbacks<Trailer[]> {

        private class TrailersResult
        {
            Trailer[] results;
        }

        @Override
        public Loader<Trailer[]> onCreateLoader(int id, final Bundle args) {
            return new AsyncTaskLoader<Trailer[]>(DetailsActivity.this) {

                private Trailer[] mCachedTrailers;

                @Override
                protected void onStartLoading() {
                    if (args == null) {
                        Log.d(TAG, "TrailersLoader - onStartLoading - no args");
                        return;
                    }

                    if (mCachedTrailers == null) {
                        mLoading.setVisibility(View.VISIBLE);
                        Log.d(TAG, "TrailersLoader - onStartLoading - no cache");
                        forceLoad();
                    }
                    else {
                        Log.d(TAG, "TrailersLoader - onStartLoading - cache");
                        deliverResult(mCachedTrailers);
                    }
                }

                @Override
                public Trailer[] loadInBackground() {
                    Log.d(TAG, "TrailersLoader - loadInBackground");

                    String url = args.getString(MOVIE_TRAILERS_URL_EXTRA);

                    if (TextUtils.isEmpty(url)) {
                        return null;
                    }

                    try {
                        return mGson.fromJson(NetworkUtils.getResponseFromHttpUrl(new URL(url)), TrailersResult.class).results;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                public void deliverResult(Trailer[] trailers) {
                    Log.d(TAG, "TrailersLoader - deliverResult: ");
                    mCachedTrailers = trailers;
                    super.deliverResult(mCachedTrailers);
                }
            };
        }

        @Override
        public void onLoadFinished(Loader<Trailer[]> loader, Trailer[] trailers) {
            Log.d(TAG, "onLoadFinished");

            mLoading.setVisibility(View.INVISIBLE);

            if (trailers != null) {
                showTrailers(trailers);
            } else {
                showErrorMessage(MOVIE_TRAILERS_LOADER);
            }
        }

        @Override
        public void onLoaderReset(Loader<Trailer[]> loader) {

        }
    }

    private void fetchTrailersForMovie(int movieId) {
        if (!NetworkUtils.isOnline(this))
            return;

        URL url = NetworkUtils.getTrailersUrl(String.valueOf(movieId));

        if (url != null) {
            Bundle queryBundle = new Bundle();
            queryBundle.putString(MOVIE_TRAILERS_URL_EXTRA, url.toString());

            getSupportLoaderManager().restartLoader(MOVIE_TRAILERS_LOADER, queryBundle, mTrailersLoader);
        }
    }

    private void fetchReviewsForMovie(int movieId) {
        if (!NetworkUtils.isOnline(this))
            return;

        URL url = NetworkUtils.getReviewsUrl(String.valueOf(movieId));

        if (url != null) {
            Bundle queryBundle = new Bundle();
            queryBundle.putString(MOVIE_REVIEWS_URL_EXTRA, url.toString());

            getSupportLoaderManager().restartLoader(MOVIE_REVIEWS_LOADER, queryBundle, mReviewsLoader);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        ButterKnife.bind(this);

        Intent intent = getIntent();

        if (intent != null && intent.hasExtra(Movie.MOVIE_PARCELABLE_KEY))
        {
            Movie movie = intent.getParcelableExtra(Movie.MOVIE_PARCELABLE_KEY);

            if (movie != null) {
                Log.d(TAG, "MovieDetailsActivity - movie: " + movie.toString());

                mOriginalTitle.setText(movie.getOriginalTitle());

                NetworkUtils.fetchAndLoadImage(this
                        , NetworkUtils.getFullImageUrl(movie.getPosterPath())
                        , mMoviePoster);

                mPlotSynopsis.setText(movie.getOverview().trim());
                mUserRating.setRating(movie.getRating()/10 * 5);
                mReleaseDate.setText(DateFormat.format("d MMMM yyyy", movie.getReleaseDate()));

                Cursor existsCursor = getContentResolver().query(FavoriteMoviesProvider.FavoriteMovies.CONTENT_URI, null, FavoriteMoviesColumns.ID + " = " + movie.getId(), null, null);
                if (existsCursor != null && existsCursor.getCount() > 0) {
                    mFavorite.setChecked(true);
                    mFavorite.setText(getString(R.string.mark_unfavorite));
                    existsCursor.close();
                }

                mMovie = movie;

                {
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
                    mTrailersRecyclerView.setLayoutManager(linearLayoutManager);
                    mTrailersRecyclerView.setHasFixedSize(true);

                    mTrailersRecyclerView.addItemDecoration(new DividerItemDecoration(this, linearLayoutManager.getOrientation()));

                    mTrailersAdapter = new TrailersAdapter(this, this);
                    mTrailersRecyclerView.setAdapter(mTrailersAdapter);
                }

                {
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
                    mReviewsRecyclerView.setLayoutManager(linearLayoutManager);
                    mReviewsRecyclerView.setHasFixedSize(true);

                    mReviewsRecyclerView.addItemDecoration(new DividerItemDecoration(this, linearLayoutManager.getOrientation()));

                    mReviewsAdapter = new ReviewsAdapter(this, this, mReviewsRecyclerView);
                    mReviewsRecyclerView.setAdapter(mReviewsAdapter);
                }

                fetchReviewsForMovie(movie.getId());
                fetchTrailersForMovie(movie.getId());
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void viewTrailer(String id) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + id));

        if (appIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(appIntent);
        } else {
            startActivity(webIntent);
        }
    }

    @OnClick(R.id.mark_as_favorite)
    public void markedAsFavoriteClicked(CheckBox favorite) {
        boolean markedAsFavorite = favorite.isChecked();

        if (markedAsFavorite) {
            Log.d(TAG, "Faved! ~> " + mMovie.getOriginalTitle());
            ContentValues cv = new ContentValues();
            cv.put(FavoriteMoviesColumns.ID, mMovie.getId());
            cv.put(FavoriteMoviesColumns.TITLE, mMovie.getOriginalTitle());
            cv.put(FavoriteMoviesColumns.DESCRIPTION, mMovie.getOverview());
            cv.put(FavoriteMoviesColumns.RATING, mMovie.getRating());
            cv.put(FavoriteMoviesColumns.RELEASE_DATE, mMovie.getReleaseDate().getTime() / 1000L);
            cv.put(FavoriteMoviesColumns.POSTER_PATH, mMovie.getPosterPath());

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ((BitmapDrawable)mMoviePoster.getDrawable()).getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, bos);
            cv.put(FavoriteMoviesColumns.POSTER_DATA, bos.toByteArray());

            cv.put(FavoriteMoviesColumns.FAVORITE_TIME, PopularMovies.getUnixTimestamp());
            getContentResolver().insert(FavoriteMoviesProvider.FavoriteMovies.CONTENT_URI, cv);
        } else {
            Log.d(TAG, "Unfaved! ~> " + mMovie.getOriginalTitle());
            getContentResolver().delete(FavoriteMoviesProvider.FavoriteMovies.withId(mMovie.getId()), null, null);
        }
    }
}
