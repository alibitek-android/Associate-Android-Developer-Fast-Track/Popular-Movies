package ro.linuxgeek.popularmovies;

import android.app.Application;

import com.facebook.stetho.Stetho;


public class PopularMovies extends Application {
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }

    public static long getUnixTimestamp() {
        return System.currentTimeMillis() / 1000L;
    }
}
