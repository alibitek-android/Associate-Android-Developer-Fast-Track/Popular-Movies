package ro.linuxgeek.popularmovies.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import ro.linuxgeek.popularmovies.BuildConfig;

public final class NetworkUtils {

    private static final String TAG = NetworkUtils.class.getSimpleName();

    private static final String MOVIEDB_IMAGE_BASE_URL = "https://image.tmdb.org/t/p/";
    private static final String MOVIEDB_IMAGE_FILE_SIZE = "w342";

    private static final String API_KEY_PARAM= "api_key";
    private static final String LANGUAGE_PARAM = "language";
    private static final String LANGUAGE_PARAM_DEFAULT_VALUE = "en-US";
    private static final String MOVIEDB_API_BASE_URL = "https://api.themoviedb.org/3";

    private static final String MOVIEDB_API_MOVIE_BASE_URI = "movie";

    private static final String MOVIEDB_API_POPULAR_MOVIES_URI = "movie/popular";
    private static final String MOVIEDB_API_TOP_RATED_MOVIES_URI = "movie/top_rated";

    // Related videos for a selected movie
    private static final String MOVIEDB_API_MOVIE_RELATED_VIDEOS_URI = "videos";

    // User reviews for a selected movie
    private static final String MOVIEDB_API_MOVIE_USER_REVIEWS_URI = "reviews";

    private static OkHttpClient httpClient = new OkHttpClient();

    /**
     * Load an image from the specified URL into the given ImageView.
     * Picasso will handle loading the images on a background thread, image decompression and caching the images.
     * @param context the context
     * @param url the url to fetch the image data from
     * @param targetImage the image view to show the data into
     */
    public static void fetchAndLoadImage(Context context, String url, ImageView targetImage) {
        Picasso.with(context).load(url).into(targetImage);
    }

    /**
     * Get the full URL of the image from the filePath obtained from the API
     * @param filePath the request URI filePath for the image
     * @return the full URL to the image
     * @see <a href="https://developers.themoviedb.org/4/getting-started/images">https://developers.themoviedb.org/4/getting-started/images</a>
     */
    public static String getFullImageUrl(String filePath) {
        return MOVIEDB_IMAGE_BASE_URL + MOVIEDB_IMAGE_FILE_SIZE + filePath;
    }

    public static URL getPopularMoviesUrl() {
        Uri builtUri = Uri.parse(MOVIEDB_API_BASE_URL).buildUpon()
                .appendEncodedPath(MOVIEDB_API_POPULAR_MOVIES_URI)
                .appendQueryParameter(API_KEY_PARAM, BuildConfig.MOVIEDB_API_KEY)
                .appendQueryParameter(LANGUAGE_PARAM, LANGUAGE_PARAM_DEFAULT_VALUE).build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Popular Movies URL: " + url);

        return url;
    }

    public static URL getTopRatedMoviesUrl() {
        Uri builtUri = Uri.parse(MOVIEDB_API_BASE_URL).buildUpon()
                .appendEncodedPath(MOVIEDB_API_TOP_RATED_MOVIES_URI)
                .appendQueryParameter(API_KEY_PARAM, BuildConfig.MOVIEDB_API_KEY)
                .appendQueryParameter(LANGUAGE_PARAM, LANGUAGE_PARAM_DEFAULT_VALUE).build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "TopRated Movies URL: " + url);

        return url;
    }

    public static URL getTrailersUrl(String movieId) {
        Uri builtUri = Uri.parse(MOVIEDB_API_BASE_URL).buildUpon()
                .appendEncodedPath(MOVIEDB_API_MOVIE_BASE_URI)
                .appendEncodedPath(movieId)
                .appendEncodedPath(MOVIEDB_API_MOVIE_RELATED_VIDEOS_URI)
                .appendQueryParameter(API_KEY_PARAM, BuildConfig.MOVIEDB_API_KEY)
                .appendQueryParameter(LANGUAGE_PARAM, LANGUAGE_PARAM_DEFAULT_VALUE).build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if (url != null) {
            Log.d(TAG, "Trailers URL: " + url.toString());
        }

        return url;
    }

    public static URL getReviewsUrl(String movieId) {
        Uri builtUri = Uri.parse(MOVIEDB_API_BASE_URL).buildUpon()
                .appendEncodedPath(MOVIEDB_API_MOVIE_BASE_URI)
                .appendEncodedPath(movieId)
                .appendEncodedPath(MOVIEDB_API_MOVIE_USER_REVIEWS_URI)
                .appendQueryParameter(API_KEY_PARAM, BuildConfig.MOVIEDB_API_KEY)
                .appendQueryParameter(LANGUAGE_PARAM, LANGUAGE_PARAM_DEFAULT_VALUE).build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if (url != null) {
            Log.d(TAG, "Reviews URL: " + url.toString());
        }

        return url;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    public static String getResponseFromHttpUrl(URL url) throws IOException {
        Request request = new Request.Builder()
                .addHeader("Accept-Charset", "UTF-8")
                .addHeader("Content-Type", "application/json;charset=utf-8")
                .url(url)
                .build();

        try (Response response = httpClient.newCall(request).execute()) {
            return response.body().string();
        }
    }
}
