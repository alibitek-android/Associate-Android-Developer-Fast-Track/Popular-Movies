# Popular Movies - Stage 2: Trailers, Reviews, and Favorites

## Project Overview
Welcome back to Popular Movies! In this second and final stage, you’ll add additional functionality to the app you built in Stage 1.

## Requirements
Add more information to your movie details view:  
- You’ll allow users to view and play trailers ( either in the youtube app or a web browser).  
- You’ll allow users to read reviews of a selected movie.  
- You’ll also allow users to mark a movie as a favorite in the details view by tapping a button(star). This is for a local movies collection that you will maintain and does not require an API request*.  
- You’ll modify the existing sorting criteria for the main view to include an additional pivot to show their favorites collection.  
- Lastly, you’ll optimize your app experience for tablet.  

## Implementation Guide
For step-by-step support, we've provided details on how to approach each task in this [Implementation Guide](https://docs.google.com/document/d/1ZlN1fUsCSKuInLECcJkslIqvpKlP7jWL2TP9m6UiA6I/pub?embedded=true#h.7sxo8jefdfll).

## Evaluation Rubric
Your project will be evaluated by a Udacity Code Reviewer according to [this rubric for Stage 2](https://review.udacity.com/#!/rubrics/67/view).

## [Udacity Android Developer Nanodegree - Core App Quality Guidelines](https://udacity.github.io/android-nanodegree-guidelines/core.html)

## [Clean Up Your Android Project Before Review](https://docs.google.com/document/d/1eYvuXY7GRE6VQpq4Rp-KotU1ti-JEySN1KdyKwjhzEQ/pub?embedded=true)

## How to build
1. Define `MOVIEDB_API_KEY` inside [gradle.properties](./gradle.properties)
2. Run `./gradlew runApp`

<div style="text-align:center">
<img src="https://www.themoviedb.org/assets/static_cache/27b65cb40d26f78354a4ac5abf87b2be/images/v4/logos/powered-by-rectangle-green.svg" width="250" height="100" />
</div>